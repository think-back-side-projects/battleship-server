const WebSocket = require('ws');
const WSSocket = require('./WSSocket');

/**
 * Class WebSocket manager
 * @module WSManager
 */
class WSManager {
  /**
   * Creates an instance of WSManager
   * @param {String} path
   * @param {Number} port
   */
  constructor(path, port) {
    /**
     * The desired path for the WebSocket server
     * @type {String}
     */
    this._path = path;
    /**
     * The desired port for the WebSocket server
     * @type {Number}
     */
    this._port = port;

    /**
     * The websocket server object
     * @type {WebSocket}
     */
    this._webSocketServer = null;

    /**
     * Collection of sockets
     * @type {Array<WSSocket>}
     */
    this._sockets = [];
  }

  /**
   * @return {WebSocket}
   * @method getWebSocketServer
   */
  getWebSocketServer() {
    if (this._webSocketServer === null) {
      this._webSocketServer = new WebSocket.Server({path: this._path, port: this._port});
    }

    return this._webSocketServer;
  }

  /**
   * Initializes the Websocket server
   * @method start
   */
  start() {
    const wss = this.getWebSocketServer();

    wss.on('connection', this._wssOnConnection.bind(this));
    wss.on('error', this._wssOnError.bind(this));
    wss.on('close', this._wssOnClose.bind(this));
  }

  /**
   * On connection event, stores the new socket.
   * @method _wssOnConnection
   * @param {WebSocket} socket
   * @private
   */
  _wssOnConnection(socket) {
    this._sockets.push(new WSSocket(socket));
  }

  /**
   * @method _wssOnError
   * @param {Error} error
   * @private
   */
  _wssOnError(error) {}

  /**
   * @method _wssOnClose
   * @param {Error} error
   * @private
   */
  _wssOnClose(error) {}
}

module.exports = WSManager;
