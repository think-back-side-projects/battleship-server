const uuidv1 = require('uuid/v1');

/**
 * Class WebSocket manager
 * @module WSManager
 */
class WSSocket {
  /**
   * Creates an instance of WSSocket and assigns
   * it an uuid.
   * @param {WebSocket} socket
   */
  constructor(socket) {
    /**
     * @type {String}
     */
    this.uuid = uuidv1();

    /**
     * @type {WebSocket}
     */
    this.socket = socket;

    /**
     * @type {Array}
     */
    this._unverifiedMessages = [];

    this.socket.on('message', this._receivedMessage.bind(this));
  }

  /**
   * Send message to socket
   * @method send
   * @param {String} message
   * @param {String} type
   * @param {Boolean=} verifyAck
   */
  send(message, type, verifyAck=true) {
    const formattedMessage = this._createMessage(message, type);

    this.socket.send(JSON.stringify(formattedMessage));

    if (verifyAck) {
      this._unverifiedMessages.push(formattedMessage);
    }
  }

  /**
   * Send message to socket
   * @private
   * @method _receivedMessage
   * @param {String} message
   */
  _receivedMessage(message) {}

  /**
   * Send message to socket
   * @private
   * @method _createMessage
   * @param {String} message
   * @param {String} type
   * @return {String}
   */
  _createMessage(message, type) {
    return {
      id: uuidv1(),
      type,
      message,
    };
  }

  /**
   * Send message to socket
   * @private
   * @method _waitAck
   * @param {String} message
   */
  _waitAck(message) {
    setInterval(() => {
      if (this._unverifiedMessages.filter((msg) => message.id === msg.id)) {}
    }, 10000);
  }
}

module.exports = WSSocket;
